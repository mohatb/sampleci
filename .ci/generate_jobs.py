import os
import sys
from jinja2 import Template

SUBPIPELINES = int(os.environ['SUBPIPELINES']) if "SUBPIPELINES" in os.environ else sys.exit("Missing ${SUBPIPELINES} variable")
ITERATIONS = int(os.environ['ITERATIONS']) if "ITERATIONS" in os.environ else sys.exit("Missing ${ITERATIONS} variable")

SUBPIPELINES = range(SUBPIPELINES)
ITERATIONS = range(ITERATIONS)

with open('.ci/jobs.j2') as file:
    template = Template(file.read())

generated_jobs = template.render(subpipelines=SUBPIPELINES, iterations=ITERATIONS)

with open('generated_jobs.yaml', 'w') as file:
    file.write(generated_jobs)
