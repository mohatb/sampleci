# K8s unexpected EOF debug

## Description

This script spawns many, but very basic jobs. The idea is that more jobs give higher chance for unexpected EOF to appear, but the job itself doesn't have to be demanding. Thus, all these jobs only print "hello".

## Settings
Edit the .gitlab-ci.yml file in order to adjust how many jobs you want to spawn.

variables:
  SUBPIPELINES: "5"
  ITERATIONS: "50"

SUBPIPELINES - this is the amount of cocurrent jobs running at the same time.
ITERATIONS - how many jobs to create within subpipeline.

With the settings as above, a total of 250 jobs will be created, and up to five of them will run at one time. Note that some subpipelines may finish a little bit faster than other.

If you desire to run the jobs for long time (for example, during the night), an absurbly high number of iterations should be inserted, perhaps even in thousdans. Please keep in mind that I did not verify Gitlab limits. With very high numbers it is quite probable to hit various possible limitations, such as timeout or maximum number of jobs in one pipeline.
